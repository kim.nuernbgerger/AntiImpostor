// Thanks for the levenstein implementation, SO!
// https://stackoverflow.com/a/36566052

function editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();

    var costs = new Array();
    for (var i = 0; i <= s1.length; i++) {
        var lastValue = i;
        for (var j = 0; j <= s2.length; j++) {
            if (i == 0)
                costs[j] = j;
            else {
                if (j > 0) {
                    var newValue = costs[j - 1];
                    if (s1.charAt(i - 1) != s2.charAt(j - 1))
                        newValue = Math.min(Math.min(newValue, lastValue),
                            costs[j]) + 1;
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }
        }
        if (i > 0)
            costs[s2.length] = lastValue;
    }
    return costs[s2.length];
}

function similarity(s1, s2) {
    var longer = s1;
    var shorter = s2;
    if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
    }
    var longerLength = longer.length;
    if (longerLength == 0) {
        return 1.0;
    }
    return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}

// Thanks for the simple image comparison, rosettacode
// https://rosettacode.org/wiki/Percentage_difference_between_images#JavaScript

function getImageData(url, callback) {
    var img = document.createElement('img');
    var canvas = document.createElement('canvas');

    img.onload = function () {
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        callback(ctx.getImageData(0, 0, img.width, img.height));
    };

    img.crossOrigin = "Anonymous";
    img.src = url;
}

function getImageSimilarity(firstImage, secondImage, callback) {
    getImageData(firstImage, function (img1) {
        getImageData(secondImage, function (img2) {
            if (img1.width !== img2.width || img1.height != img2.height) {
                callback(NaN);
                return;
            }

            var diff = 0;

            for (var i = 0; i < img1.data.length / 4; i++) {
                diff += Math.abs(img1.data[4 * i + 0] - img2.data[4 * i + 0]) / 255;
                diff += Math.abs(img1.data[4 * i + 1] - img2.data[4 * i + 1]) / 255;
                diff += Math.abs(img1.data[4 * i + 2] - img2.data[4 * i + 2]) / 255;
            }

            callback(1 - diff / (img1.width * img1.height * 3));
        });
    });
}


// ----
// Put it all together
// ----

// Setup thresholds
var thresholdScreenName = 0.5;
var thresholdName = 0.6;
var thresholdImage = 0.7;

function checkImposator(orgTweet, suspectTweet)
{
    var orgScreenName = orgTweet.data("screen-name");
    var orgName = orgTweet.data("name");
    var orgVerified = orgTweet.hasClass("with-social-proof");
    var orgImage = orgTweet.find("img.avatar").attr("src");

    var suspectScreenName = suspectTweet.data("screen-name");
    var suspectName = suspectTweet.data("name");
    var suspectVerified = suspectTweet.hasClass("with-social-proof");
    var suspectImage = orgTweet.find("img.avatar").attr("src");

    // Don't compare owner
    if(orgScreenName == suspectScreenName) return;

    // Trust twitter.
    if(suspectVerified) return;

    var screenNameSimilarity = similarity(orgScreenName, suspectScreenName);
    var nameSimilarity = similarity(orgName, suspectName);

    // Org is verified, suspect is not and seems to copy org? Bye, bye!
    if(orgVerified && (screenNameSimilarity > thresholdScreenName || nameSimilarity > thresholdName))
    {
        getRidOfImposator(suspectTweet);
        return;
    }

    // Org is not verified, but our suspect has almost the same names
    if(screenNameSimilarity > thresholdScreenName && nameSimilarity > thresholdName)
    {
        getRidOfImposator(suspectTweet);
        return;
    }

    // If either the screenName, or the name seems to reflect an impostor, check the image
    if(screenNameSimilarity > thresholdScreenName || nameSimilarity > thresholdName)
    {
        getImageSimilarity(orgImage, suspectImage, function(similarity) {
            if(similarity > thresholdImage)
            {
                getRidOfImposator(suspectTweet);
            }
        });
    }
}

function getRidOfImposator(tweet)
{
    var tweetContainer = tweet.closest("div.ThreadedConversation-tweet");
    if(tweetContainer.hasClass("last"))
        tweetContainer.prev().addClass("last");
    tweetContainer.remove();
}

(function(){
    // One round of checking for all pre-loaded tweets
    var orgTweet = $("div.tweet.js-original-tweet");
    var allReplies = $("div.tweet:not(.div.tweet.js-original-tweet,div.tweet.RetweetDialog-tweet)");
    allReplies.each(function (i, x) { checkImposator(orgTweet, $(x)); });

    // Same game for every tweet that is loaded on demand
    onNodeInsertEvent = function(event) {
        if (event.animationName == 'nodeInserted')
        {
            var orgTweet = $("div.tweet.js-original-tweet");
            var suspectTweet = $(event.target).find("div.tweet");
            checkImposator(orgTweet, suspectTweet);
        }
    }

    // Thanks to whomever figured this this awful,
    // yet beautiful "node insertion" event hack. <3
    document.addEventListener('animationstart', onNodeInsertEvent, false);
    document.addEventListener('MSAnimationStart', onNodeInsertEvent, false);
    document.addEventListener('webkitAnimationStart', onNodeInsertEvent, false);
})();